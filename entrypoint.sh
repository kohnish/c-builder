#!/bin/bash
if [[ -z "$1" ]]; then
    exec /bin/bash
else
    exec "$@"
fi
